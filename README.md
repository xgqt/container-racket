# Container-Racket

Racket containers, mainly for CI/CD.

## Usage

### Build

Go to the desired image directory inside `Source`
and execute:

``` shell
podman build --tag "$(basename "$(pwd)")" .
```

### Cleanup

``` shell
podman rmi "$(podman images --filter dangling=true --quiet)"
```
